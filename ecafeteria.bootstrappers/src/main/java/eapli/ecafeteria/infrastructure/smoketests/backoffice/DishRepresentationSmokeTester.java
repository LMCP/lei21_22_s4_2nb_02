/*
 * Copyright (c) 2013-2022 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and
 * associated documentation files (the "Software"), to deal in the Software
 * without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish,
 * distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom
 * the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eapli.ecafeteria.infrastructure.smoketests.backoffice;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import eapli.ecafeteria.dishmanagement.application.ListDishService;
import eapli.ecafeteria.dishmanagement.domain.Dish;
import eapli.framework.actions.Action;
import eapli.framework.representations.builders.JsonRepresentationBuilder;
import eapli.framework.strings.FormattedString;
import eapli.framework.strings.PrettyJsonString;
import eapli.framework.strings.PrettyXmlString;
import eapli.framework.validations.Invariants;

/**
 *
 * @author Paulo Gandra de Sousa
 *
 */
public class DishRepresentationSmokeTester implements Action {
    private static final Logger LOGGER = LogManager.getLogger(DishRepresentationSmokeTester.class);

    private final ListDishService svc = new ListDishService();

    @Override
    public boolean execute() {
        testDishJsonRepresentation();

        testDishJsonMapping();
        testDishXmlMapping();

        // nothing else to do
        return true;
    }

    /**
     * Get a JSON representation based on jackson annotations. This is ideal for scenarios where
     * there is only one representation needed for all use cases and we want to take advantage of a
     * library to avoid coding errors.
     */
    private void testDishJsonMapping() {
        final Iterable<Dish> l = svc.allDishes();
        Invariants.nonNull(l);

        final var dish = l.iterator().next();

        final FormattedString json = PrettyJsonString.fromObject(dish);
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("-- DISH Mapping to JSON --");
            LOGGER.info(json.toString());
        }
    }

    /**
     * Get an XML representation based on JAX-B annotations. This is ideal for scenarios where
     * there is only one representation needed for all use cases and we want to take advantage of a
     * library to avoid coding errors.
     */
    private void testDishXmlMapping() {
        final Iterable<Dish> l = svc.allDishes();
        Invariants.nonNull(l);

        final var dish = l.iterator().next();

        final FormattedString xml = PrettyXmlString.fromObject(dish, Dish.class);
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("-- DISH Mapping to XML --");
            LOGGER.info(xml.toString());
        }
    }

    /**
     * Get a JSON representation using the {@link eapli.framework.representations.Representationable
     * Representationable} interface. This is ideal for scenarios where there might be the need to
     * get different representations for different use cases. In such scenarios we cannot rely on a
     * single DTO or automatic mapping thru Jackson/JAX-B.
     *
     * @param dish
     */
    private void testDishJsonRepresentation() {
        final Iterable<Dish> l = svc.allDishes();
        Invariants.nonNull(l);

        final var dish = l.iterator().next();

        final var builder = new JsonRepresentationBuilder();
        final String json = dish.buildRepresentation(builder);
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("-- DISH JSON --");
            LOGGER.info(json);
            LOGGER.info("-- DISH Pretty JSON --");
            LOGGER.info(PrettyJsonString.fromString(json).toString());
        }
    }
}
