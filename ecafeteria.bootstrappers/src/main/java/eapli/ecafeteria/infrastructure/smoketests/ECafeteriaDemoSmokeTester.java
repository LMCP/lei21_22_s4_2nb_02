/*
 * Copyright (c) 2013-2022 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eapli.ecafeteria.infrastructure.smoketests;

import eapli.ecafeteria.infrastructure.smoketests.backoffice.AllergenImageSmokeTester;
import eapli.ecafeteria.infrastructure.smoketests.backoffice.ConnectionPoolingSmokeTester;
import eapli.ecafeteria.infrastructure.smoketests.backoffice.DishExportSmokeTester;
import eapli.ecafeteria.infrastructure.smoketests.backoffice.DishManagementSmokeTester;
import eapli.ecafeteria.infrastructure.smoketests.backoffice.DishManagementViaDTOSmokeTester;
import eapli.ecafeteria.infrastructure.smoketests.backoffice.DishRepresentationSmokeTester;
import eapli.ecafeteria.infrastructure.smoketests.backoffice.DishTypeManagementSmokeTester;
import eapli.ecafeteria.infrastructure.smoketests.backoffice.DishesAndAllergensSmokeTester;
import eapli.framework.actions.Action;
import eapli.framework.actions.ChainedAction;

/**
 * Execute simple smoke tests on the application layer. We are assuming that the
 * bootstrappers mainly test the "register" use cases and some of the "finders"
 * to support those "register", so these smoke tests will focus mainly on executing the
 * other application methods.
 *
 * @author Paulo Gandra de Sousa
 */
@SuppressWarnings({ "squid:S1126", "java:S106" })
public class ECafeteriaDemoSmokeTester implements Action {

    @Override
    public boolean execute() {
        System.out.println("\n\n------- SPECIFIC FEATURES -------");

        return ChainedAction.first(new DishTypeManagementSmokeTester())
                .then(new DishManagementSmokeTester())
                .then(new DishManagementViaDTOSmokeTester())
                .then(new DishRepresentationSmokeTester())
                .then(new DishExportSmokeTester())
                .then(new DishesAndAllergensSmokeTester())
                .then(new AllergenImageSmokeTester())
                .then(new ConnectionPoolingSmokeTester())
                .execute();
    }
}
