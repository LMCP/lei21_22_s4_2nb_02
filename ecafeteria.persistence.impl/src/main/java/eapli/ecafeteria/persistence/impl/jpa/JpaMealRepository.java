/*
 * Copyright (c) 2013-2022 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eapli.ecafeteria.persistence.impl.jpa;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;

import eapli.ecafeteria.mealmanagement.domain.Meal;
import eapli.ecafeteria.mealmanagement.domain.MealType;
import eapli.ecafeteria.mealmanagement.repositories.MealRepository;

/**
 * @author Paulo Gandra Sousa
 *
 */
public class JpaMealRepository extends CafeteriaJpaRepositoryBase<Meal, Long, Long>
        implements MealRepository {

    public JpaMealRepository() {
        super("id");
    }

    @Override
    public Iterable<Meal> findByDay(final Calendar forDay) {
        final TypedQuery<Meal> q = createQuery("SELECT e FROM Meal e WHERE  e.day = :forDay",
                Meal.class);
        q.setParameter("forDay", forDay, TemporalType.DATE);
        return q.getResultList();
    }

    @Override
    public Iterable<Meal> findByPeriod(final Calendar beginDate, final Calendar endDate) {
        final TypedQuery<Meal> q = createQuery(
                "SELECT e FROM Meal e WHERE e.day BETWEEN :beginDate AND :endDate",
                Meal.class);
        q.setParameter("beginDate", beginDate, TemporalType.DATE);
        q.setParameter("endDate", endDate, TemporalType.DATE);
        return q.getResultList();
    }

    @Override
    public Iterable<Meal> findNotYetCookedByDay(final Calendar ofDay, final MealType mealType) {
        final Map<String, Object> params = new HashMap<>();
        params.put("ofDay", ofDay);
        params.put("mealType", mealType);
        return match(
                "e.day = :ofDay AND e.mealType = :mealType AND e NOT IN (SELECT c.meal FROM CookedMealQty c)",
                params);
    }

    @Override
    public Iterable<Meal> findByDayAndType(final Calendar theDay, final MealType mealType) {
        final TypedQuery<Meal> q = createQuery(
                "SELECT e FROM Meal e WHERE  e.day = :forDay AND e.mealType = :type",
                Meal.class);
        q.setParameter("forDay", theDay, TemporalType.DATE);
        q.setParameter("type", mealType);
        return q.getResultList();
    }
}
