/*
 * Copyright (c) 2013-2022 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and
 * associated documentation files (the "Software"), to deal in the Software
 * without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish,
 * distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom
 * the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eapli.ecafeteria.infrastructure.smoketests.backoffice;

import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import eapli.ecafeteria.dishmanagement.application.ActivateDeactivateDishController;
import eapli.ecafeteria.dishmanagement.application.ChangeDishController;
import eapli.ecafeteria.dishmanagement.domain.Dish;
import eapli.ecafeteria.dishmanagement.domain.NutricionalInfo;
import eapli.ecafeteria.infrastructure.bootstrapers.TestDataConstants;
import eapli.ecafeteria.reporting.dishes.application.DishReportingController;
import eapli.ecafeteria.reporting.dishes.dto.DishesPerCaloricCategory;
import eapli.ecafeteria.reporting.dishes.dto.DishesPerDishType;
import eapli.framework.actions.Action;
import eapli.framework.general.domain.model.Money;
import eapli.framework.validations.Invariants;

/**
 *
 * @author Paulo Gandra de Sousa
 *
 */
public class DishManagementSmokeTester implements Action {
    private static final Logger LOGGER = LogManager.getLogger(DishManagementSmokeTester.class);

    private final ChangeDishController changeDishController = new ChangeDishController();
    private final ActivateDeactivateDishController activateDishController = new ActivateDeactivateDishController();
    private final DishReportingController dishReportingController = new DishReportingController();

    @Override
    public boolean execute() {
        testActivateDeactivateDish();
        testFindAllAndChangeDish();
        testSearchAndChangeDish();
        testSearchAndUpdateDish();
        testReportingDish();

        // nothing else to do
        return true;
    }

    private void testActivateDeactivateDish() {
        final Iterable<Dish> l = activateDishController.allDishes();
        Invariants.nonNull(l);

        var dish = l.iterator().next();

        final boolean oldStatus = dish.isActive();

        dish = activateDishController.changeDishState(dish);
        Invariants.ensure(dish.isActive() == !oldStatus);

        LOGGER.info("»»» Changed status of {} to {}", dish, dish.isActive());
    }

    /**
     * This smoke test simulates the user searching for a dish.
     */
    private void testSearchAndUpdateDish() {
        // the user searches for the desired dish
        final Dish picanha = changeDishController.searchDish(TestDataConstants.DISH_NAME_PICANHA)
                .orElseThrow(IllegalStateException::new);

        // the UI will present the dish and allow the user to enter the changed data (e.g., in a
        // data grid)
        final var newInfo = new NutricionalInfo(20, 5);
        final var newPrice = Money.euros(25);
        picanha.update(picanha.dishType(), false, newPrice, newInfo);

        // the UI calls the controller
        final var updated = changeDishController.updateDish(picanha);

        Invariants.ensure(updated.nutricionalInfo().orElseThrow(IllegalStateException::new).equals(newInfo)
                && updated.currentPrice().equals(newPrice)
                && !updated.isActive());

        LOGGER.info("»»» Updated dish {}", updated);
    }

    /**
     * This smoke test simulates the user selecting the entry in a dropdown UI.
     */
    private void testFindAllAndChangeDish() {
        final Iterable<Dish> l = changeDishController.allDishes();
        Invariants.nonNull(l);

        final var dish = l.iterator().next();

        doChangeDishAndLog(dish);
    }

    /**
     * This smoke test simulates the user searching for a dish.
     */
    private void testSearchAndChangeDish() {
        final Optional<Dish> changed = changeDishController.searchDish(TestDataConstants.DISH_NAME_PICANHA)
                .map(this::doChangeDishAndLog);
        if (!changed.isPresent()) {
            LOGGER.info("»»» The dish {} does not exist", TestDataConstants.DISH_NAME_PICANHA);
        }
    }

    private Dish doChangeDishAndLog(final Dish dish) {
        final var changedDish = doChangeDishPrice(dish);

        return doChangeDishNutricionalInfo(changedDish);
    }

    private Dish doChangeDishNutricionalInfo(final Dish dish) {
        // change nutricional info
        final var newInfo = new NutricionalInfo(10, 2);
        final var changed = changeDishController.changeDishNutricionalInfo(dish, newInfo);
        Invariants.ensure(changed.nutricionalInfo().orElseThrow(IllegalStateException::new).equals(newInfo));

        LOGGER.info("»»» Changed nutricional info of {} to {}", dish, changed.nutricionalInfo());

        return changed;
    }

    private Dish doChangeDishPrice(final Dish dish) {
        // change price
        final Money oldPrice = dish.currentPrice();
        final Money newPrice = oldPrice.add(Money.euros(1));
        final var changedPrice = changeDishController.changeDishPrice(dish, newPrice);
        Invariants.ensure(dish.currentPrice().equals(newPrice));

        LOGGER.info("»»» Changed price of {} to {}", dish, dish.currentPrice());
        return changedPrice;
    }

    private void testReportingDish() {
        final Iterable<DishesPerCaloricCategory> l1 = dishReportingController
                .reportDishesPerCaloricCategory();
        Invariants.nonNull(l1);

        final Iterable<Object[]> l2 = dishReportingController
                .reportDishesPerCaloricCategoryAsTuples();
        Invariants.nonNull(l2);

        final Iterable<DishesPerDishType> l3 = dishReportingController.reportDishesPerDishType();
        Invariants.nonNull(l3);

        final Iterable<Dish> l4 = dishReportingController.reportHighCaloriesDishes();
        Invariants.nonNull(l4);

        LOGGER.info("»»» Reporting dishes");
    }
}
